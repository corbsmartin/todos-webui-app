#--------------------------------------------------------------------------------------------------
# Make variables
#--------------------------------------------------------------------------------------------------
MANIFESTS_HOME=.generated/manifests
APP_CHART=todos-webui
APP_CHART_RELEASE=v1.0.15
APP_NAMESPACE=todos-webui

manifest:
	@mkdir -p $(MANIFESTS_HOME)/$(APP_CHART)
	@helm template $(APP_CHART) \
		-n $(APP_NAMESPACE) \
		. > $(MANIFESTS_HOME)/$(APP_CHART)/$(APP_CHART).yaml

service-account:
	oc policy add-role-to-user edit system:serviceaccount:todos-webui:gitlab-ci-sa -n todos-webui